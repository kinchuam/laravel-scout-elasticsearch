<?php

namespace Kinch\LaravelScoutElasticsearch;

use Kinch\LaravelScoutElasticsearch\Services\ElasticsearchService;
use Laravel\Scout\EngineManager;
use Illuminate\Support\ServiceProvider;
use Kinch\LaravelScoutElasticsearch\Engines\ElasticsearchEngine;

final class LaravelScoutElasticsearchServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     */
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__.'/../config/elasticsearch.php', 'elasticsearch');

        $this->app->bind(ElasticsearchService::class, function () {
            return (new ElasticsearchService());
        });
    }

    /**
     * Register the application's event listeners.
     */
    public function boot(): void
    {
        $this->app->make(EngineManager::class)->extend(ElasticSearchEngine::class, function () {
            return new ElasticSearchEngine(
                app(ElasticsearchService::class)->client(),
                config('scout.soft_delete', false)
            );
        });

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/elasticsearch.php' => config_path('elasticsearch.php'),
            ], 'config');
        }
    }

}